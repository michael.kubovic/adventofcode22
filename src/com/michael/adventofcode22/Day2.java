package com.michael.adventofcode22;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Day2 {

    private final Map<String, Integer> scoreForShape = new HashMap<>();
    private final File input;

    public static void main(String[] args) throws FileNotFoundException {
        Day2 d2 = new Day2();
        d2.part1();
        d2.part2();
    }

    public Day2() throws FileNotFoundException {
        scoreForShape.put("X", 1);
        scoreForShape.put("Y", 2);
        scoreForShape.put("Z", 3);

        input = new File("input/day2.txt");
    }

    private void part2() throws FileNotFoundException {
        /*
          A for Rock, B for Paper, and C for Scissors
          X means you need to lose,
          Y means you need to end the round in a draw,
          and Z means you need to win
         */
        Map<String, Integer> outcomes = new HashMap<>();
        outcomes.put("X", 0);
        outcomes.put("Y", 3);
        outcomes.put("Z", 6);

        // shape is XYZ
        Map<String, String> playToShape = new HashMap<>();
        playToShape.put("A X", "Z");
        playToShape.put("A Y", "X");
        playToShape.put("A Z", "Y");
        playToShape.put("B X", "X");
        playToShape.put("B Y", "Y");
        playToShape.put("B Z", "Z");
        playToShape.put("C X", "Y");
        playToShape.put("C Y", "Z");
        playToShape.put("C Z", "X");

        int score = 0;
        Scanner scanner = new Scanner(input);
        while (scanner.hasNextLine()) {
            String play = scanner.nextLine();
            String[] playParts = play.split("\\s+");

            score += outcomes.get(playParts[1]);
            String myShape = playToShape.get(play);
            score += scoreForShape.get(myShape);

//            System.out.println(score);
        }

        System.out.println("final score part2: "+score);
    }

    private void part1() throws FileNotFoundException {
        /*
          A for Rock, B for Paper, and C for Scissors
          X for Rock, Y for Paper, and Z for Scissors
         */
        Map<String, Integer> outcomes = new HashMap<>();
        outcomes.put("A X", 3);
        outcomes.put("A Y", 6);
        outcomes.put("A Z", 0);
        outcomes.put("B X", 0);
        outcomes.put("B Y", 3);
        outcomes.put("B Z", 6);
        outcomes.put("C X", 6);
        outcomes.put("C Y", 0);
        outcomes.put("C Z", 3);

        int score = 0;
        Scanner scanner = new Scanner(input);
        while (scanner.hasNextLine()) {
            String play = scanner.nextLine();
            String[] shapes = play.split("\\s+");

            score += outcomes.get(play);
            score += scoreForShape.get(shapes[1]);

//            System.out.println(score);
        }

        System.out.println("final score part1: "+score);
    }

    /**
     * abc
     * acb
     * bac
     * bca
     * cab
     * cba
     */
}
