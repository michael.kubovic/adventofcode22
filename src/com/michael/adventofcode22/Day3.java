package com.michael.adventofcode22;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.IntStream;

public class Day3 {

    private final File input;
    private final List<Character> az;

    public static void main(String[] args) throws FileNotFoundException {
        Day3 d = new Day3();
//        d.part1();
        d.part2();
    }

    Day3() {
        input = new File("input/day3.txt");

        az = IntStream
                .concat(
                    IntStream.rangeClosed('a', 'z'),
                    IntStream.rangeClosed('A', 'Z')
                )
                .mapToObj(c -> (char) c)
                .toList();
    }

    private void part2() throws FileNotFoundException {
        Scanner s = new Scanner(input);

        List<Character> badges = new ArrayList<>();
        while (s.hasNextLine()) {
            String rucksack1 = getRucksack(s);
            String rucksack2 = getRucksack(s);
            String rucksack3 = getRucksack(s);

            Map<Character, Integer> items = new HashMap<>();
            for (int i = 0; i < rucksack1.length(); i++) {
                items.merge(rucksack1.charAt(i), 1, Integer::sum);
            }
            for (int i = 0; i < rucksack2.length(); i++) {
                items.merge(rucksack2.charAt(i), 1, Integer::sum);
            }
            for (int i = 0; i < rucksack3.length(); i++) {
                items.merge(rucksack3.charAt(i), 1, Integer::sum);
            }

//            items.forEach((k, v) -> System.out.println(k + ": "+ v));

            badges.addAll(items
                            .entrySet()
                            .stream()
                            .filter(i -> i.getValue() == 3)
                            .map(Map.Entry::getKey)
                            .toList()
            );
        }

        System.out.println("All badges: "+badges);
        int sumOfBadges = badges
                .stream()
                .map(this::getPriority)
                .reduce(Integer::sum)
                .orElse(0);

//        items.forEach((k, v) -> System.out.println(k + ": "+ v));
        System.out.println("Sum of badges: "+sumOfBadges);
    }

    private String getRucksack(Scanner s) {
        return s.nextLine()
                .chars()
                .distinct()
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public void part1() throws FileNotFoundException {
        Scanner s = new Scanner(input);

        List<Character> allDuplicates = new ArrayList<>();
        while (s.hasNextLine()) {
            String rucksack = s.nextLine();

            int l = rucksack.length();
            List<Character> rucksackDuplicates = new ArrayList<>();
            for (int i = 0; i < l/2; i++) {
                for (int j = l/2; j < l; j++) {
                    char item = rucksack.charAt(i);
                    if (item == rucksack.charAt(j) && !rucksackDuplicates.contains(item)) {
                        rucksackDuplicates.add(item);
                    }
                }
            }

            System.out.println("rucksack rucksackDuplicates: "+rucksackDuplicates);
            allDuplicates.addAll(rucksackDuplicates);
        }

        System.out.println("duplicates: "+allDuplicates);

        List<Integer> priorities = allDuplicates.stream().map(this::getPriority).toList();

        System.out.println("priorities: "+priorities);
        System.out.println("priority sum: "+priorities.stream().reduce(Integer::sum));
    }

    private int getPriority(Character c) {
        return 1 + az.indexOf(c);
    }
}
