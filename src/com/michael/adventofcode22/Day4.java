package com.michael.adventofcode22;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day4
{
    private final File input = new File("input/day4.txt");
    private final Pattern linePattern = Pattern.compile("^(\\d+)-(\\d+),(\\d+)-(\\d+)$");

    public static void main(String[] args) throws FileNotFoundException
    {
        Day4 d = new Day4();
//        d.part1();
        d.part2();
    }

    private void part1() throws FileNotFoundException
    {
        final Scanner s = new Scanner(input);
        int fullyContained = 0;
        while (s.hasNextLine()) {
            String line = s.nextLine();
            final int[] l = matchLine(line);
            if ((l[0] <= l[2] && l[1] >= l[3]) || (l[1] <= l[0] && l[3] >= l[2])) {
                System.out.println("Fully contained line: " + line);
                fullyContained++;
            }
        }

        System.out.println("Fully contained pairs: " + fullyContained);
    }

    private void part2() throws FileNotFoundException
    {
        final Scanner s = new Scanner(input);
        int overlappingPairs = 0;

        outerloop:
        while (s.hasNextLine()) {
            String line = s.nextLine();
            final int[] l = matchLine(line);

            for (int i = l[0]; i <= l[1]; i++) {
                for (int j = l[2]; j <= l[3]; j++) {
                    if (i == j) {
                        System.out.println("Overlapping line: "+line+" on "+i);
                        overlappingPairs++;
                        continue outerloop;
                    }
                }
            }
        }

        System.out.println("Overlapping pairs: " + overlappingPairs);
    }

    private int[] matchLine(String line)
    {
        final Matcher matcher = linePattern.matcher(line);
        if (!matcher.find()) {
            throw new RuntimeException("Bad line");
        }

        return new int[]{
                Integer.parseInt(matcher.group(1)),
                Integer.parseInt(matcher.group(2)),
                Integer.parseInt(matcher.group(3)),
                Integer.parseInt(matcher.group(4))
        };
    }
}
