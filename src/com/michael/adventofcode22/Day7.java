package com.michael.adventofcode22;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Day7
{
    final private File input = new File("input/day7.txt");

    Map<String, Node> fs = new HashMap<>();
    Stack<Node> terminal = new Stack<>();

    public static void main(String[] args) throws FileNotFoundException {
        Day7 today = new Day7();
        today.part1();
        today.part2();
    }

    record Node (String name, int size, List<Node> children) {

        public Node(String name, int size) {
            this(name, size, new ArrayList<>());
        }

        public Node(String name) {
            this(name, 0, new ArrayList<>());
        }

        public int getSize() {
            if (children.size() > 0) {
                return children.stream().map(Node::getSize).reduce(Integer::sum).get();
            }

            return size;
        }

        public void add(Node node) {
            if (!children.contains(node)) {
                children.add(node);
            }
        }

        public boolean isDir()
        {
            return children.size() > 0;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "name='" + name + '\'' +
                    ", size=" + size +
                    ", children count=" + children.size() +
                    '}';
        }
    }

    private void part1() throws FileNotFoundException {
        final Scanner scanner = new Scanner(input);

        while (scanner.hasNextLine()) {
            String[] line = scanner.nextLine().split(" ");

            System.out.println("Line: "+ Arrays.toString(line));

            if (line[0].equals("$")) {
                switch (line[1]) {
                    case "cd" -> {
                        if (line[2].equals("..")) {
                            terminal.pop();
                        } else {
                            Node dir = getDir(line[2]);
                            terminal.push(dir);
                        }
                    }

                    case "ls" -> {
                        continue;
                    }
                }

                continue;
            }

            if (line[0].equals("dir")) {
                Node dir = getDir(line[1]);
                terminal.peek().add(dir);
            } else {
                Node file = new Node(line[1], Integer.parseInt(line[0]));
                fs.put(getPath(line[1]), file);
                terminal.peek().add(file);
            }

            System.out.println("Terminal: "+terminal);
            System.out.println("fs:" + fs);
        }

        // calculate total size for all dirs
        dirsStream().get()
                .forEach(node -> System.out.println("Dir size: "+node.name+" -> "+node.getSize()));

        int totalSize = dirsStream().get()
                .mapToInt(Node::getSize)
                .filter(s -> s <= 100000)
                .reduce(Integer::sum)
                .getAsInt();

        System.out.println("Total size is: "+totalSize);
    }

    private Supplier<Stream<Node>> dirsStream() {
        return () -> fs.values().stream().filter(Node::isDir);
    }

    private void part2() {
        // part2
        final int freeUpSize = 30000000 - (70000000 - fs.get("/").getSize());
        System.out.println("Free up size: "+freeUpSize);

        final int dirToDelete = dirsStream().get()
                .map(Node::getSize)
                .filter(s -> s >= freeUpSize)
                .min(Comparator.naturalOrder())
                .get();

        System.out.println("Dir to delete: "+dirToDelete);
    }

    private String getPath(String name) {
        if (name.equals("/")) {
            return name;
        }

        final List<String> pathParts = new ArrayList<>(terminal.stream().map(n -> n.name).toList());
        pathParts.add(name);
        pathParts.set(0, "");

        return String.join("/", pathParts);
    }

    private Node getDir(String name) {
        Node dir;
        String path = getPath(name);

        if (fs.containsKey(path)) {
            dir = fs.get(path);
        } else {
            dir = new Node(name);
            fs.put(path, dir);
        }

        return dir;
    }
}
