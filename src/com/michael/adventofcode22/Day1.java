package com.michael.adventofcode22;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Day1 {

    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("input/day1.txt");
        Scanner scanner = new Scanner(file);

        List<Integer> elves = new ArrayList<>();
        int currentCalories = 0;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.isEmpty()) {
                elves.add(currentCalories);
                currentCalories = 0;
                continue;
            }

            currentCalories += Integer.parseInt(line);
        }

        int topThreeElves = elves.stream().sorted(Comparator.reverseOrder()).limit(3).reduce(0, Integer::sum);

//        System.out.printf("Elf with most calories has %s kcal%n", maxCalories);
        System.out.printf("Top three Elves cary %s calories%n", topThreeElves);
    }
}
