package com.michael.adventofcode22;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Day6
{
    private final File input = new File("input/day6.txt");

    public static void main(String[] args) throws FileNotFoundException {
        Day6 today = new Day6();
//        today.part1();
        today.part2();
    }

    private void part2() throws FileNotFoundException {
        Scanner scanner = new Scanner(input);

        while (scanner.hasNextLine()) {
            char[] line = scanner.nextLine().toCharArray();

            for (int i = 14; i < line.length; i ++) {
                Object[] sub = IntStream.range(i - 14, i).mapToObj(j -> (char) line[j]).distinct().toArray();
                if (sub.length == 14) {
                    System.out.println("Char "+line[i]+" at " + i);
                    break;
                }
            }
        }
    }

    private void part1() throws FileNotFoundException {
        Scanner scanner = new Scanner(input);

        while (scanner.hasNextLine()) {
            char[] line = scanner.nextLine().toCharArray();

            for (int i = 3; i < line.length; i ++) {
                if (IntStream.of(line[i], line[i-1], line[i-2], line[i-3]).distinct().toArray().length == 4) {
                    System.out.println("Char "+line[i]+" at " + (i + 1));
                    break;
                }
            }
        }
    }
}
