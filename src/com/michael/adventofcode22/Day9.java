package com.michael.adventofcode22;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Day9 {

    final private File input = new File("input/day9.txt");
    private Stack<Point> headPath;
    private List<Stack<Point>> tailPaths;

    Day9() {

    }

    public static void main(String[] args) throws FileNotFoundException {
        Day9 today = new Day9();
        today.part1();
    }

    record Point(int x, int y) {
        public Point up() {
            return new Point(x, y+1);
        }

        public Point down() {
            return new Point(x, y-1);
        }

        public Point left() {
            return new Point(x-1, y);
        }

        public Point right() {
            return new Point(x+1, y);
        }

        public Point follow(Point tail) {
            if (this.equals(tail)) {
                return this;
            }

            if (tail.x == x && tail.y < y) {
                return new Point(x, y-1);
            }

            if (tail.x == x && tail.y > y) {
                return new Point(x, y+1);
            }

            if (tail.y == y && tail.x > x) {
                return new Point(x+1, y);
            }

            if (tail.y == y && tail.x < x) {
                return new Point(x-1, y);
            }

            // diagonal do nothing
            if ((tail.x == x-1 || tail.x == x+1)
                && (tail.y == y+1 || tail.y == y-1)) {
                return tail;
            }

            // far diagonal
            if ((tail.x == x-1 || tail.x == x+1)
                    && tail.y == y-2) {
                return new Point(x, y-1);
            }

            if ((tail.x == x-1 || tail.x == x+1)
                    && tail.y == y+2) {
                return new Point(x, y+1);
            }

            if ((tail.y == y-1 || tail.y == y+1)
                    && tail.x == x+2) {
                return new Point(x+1, y);
            }

            if ((tail.y == y-1 || tail.y == y+1)
                    && tail.x == x-2) {
                return new Point(x-1, y);
            }

            if (tail.y < y) {
                if (tail.x < x) {
                    return new Point(x-1, y-1);
                }

                return new Point(x+1, y-1);
            }

            if (tail.y > y) {
                if (tail.x < x) {
                    return new Point(x-1, y+1);
                }

                return new Point(x+1, y+1);
            }

            throw new RuntimeException("Cannot follow: "+this+" with "+tail);
        }
    }

    private void part1() throws FileNotFoundException {
        Scanner scanner = new Scanner(input);

        Point start = new Point(200 ,200); // 0, 0 could underflow
        headPath = new Stack<>();
        headPath.push(start);

        tailPaths = new ArrayList<>();

        for (int i = 0; i < 9; i++) {
            Stack<Point> tailPath = new Stack<>();
            tailPath.push(start);
            tailPaths.add(tailPath);
        }

        while (scanner.hasNextLine()) {
            String fullLine = scanner.nextLine();
            System.out.println("Line: " + fullLine);
            String[] line = fullLine.split(" ");

            for (int i = 0; i < Integer.parseInt(line[1]); i++) {
                Point lastHead = headPath.peek();
                Point newHead = newHead(line[0].charAt(0), lastHead);
                headPath.push(newHead);
                System.out.println("New head: " + lastHead + " -> " + newHead);
//                print();

                int index = 0;
                Point prevTail = null;
                for (Stack<Point> tailPath: tailPaths) {
                    Point origin = prevTail != null ? prevTail : newHead;

                    Point newTail;
                    try {
                        newTail = origin.follow(tailPath.peek());
                    } catch (RuntimeException e) {
                        print();

                        throw e;
                    }

                    tailPath.push(newTail);
                    System.out.println("New tail "+(index+1)+": " + origin + " -> "+newTail);
//                    print();
                    index++;

                    prevTail = newTail;
                }
            }
        }

        System.out.println("tail path length:"+ tailPaths.get(8).stream().distinct().count());
    }

    private Point newHead(char move, Point lastHead) {
        return switch (move) {
            case 'R':
                yield lastHead.right();
            case 'L':
                yield lastHead.left();
            case 'U':
                yield lastHead.up();
            case 'D':
                yield lastHead.down();
            default:
                throw new RuntimeException("Invalid move: "+ move);
        };
    }

    private void print()
    {
        int maxX = headPath.stream().map(p -> p.x).max(Comparator.naturalOrder()).get();
        int maxY = headPath.stream().map(p -> p.y).max(Comparator.naturalOrder()).get();

        String[][] map = new String[maxY + 1][maxX + 1];

        for (int y = 0; y < map.length; y++) {
            Arrays.fill(map[y], ".");
        }

        Point head = headPath.peek();
        map[head.y][head.x] = "H";

        int tailIndex = 1;
        for (Stack<Point> tailPath: tailPaths) {
            Point tail = tailPath.peek();
            String cell = map[tail.y][tail.x];

            if (cell == "H") {
                tailIndex++;
                continue;
            }

            if (cell == "." || Integer.parseInt(cell) > tailIndex) {
                map[tail.y][tail.x] = String.valueOf(tailIndex++);
            }
        }

        for (int y = map.length - 1; y >= 0; y--) {
            System.out.println(String.join("", map[y]));
        }

    }
}
