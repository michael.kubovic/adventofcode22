package com.michael.adventofcode22;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Day8 {
    final private File input = new File("input/day8.txt");

    final Tree[][] forest;

    Day8() throws FileNotFoundException {
        forest = getForest();
    }

    public static void main(String[] args) throws FileNotFoundException {
        Day8 today = new Day8();
//        today.part1();
        today.part2();
    }

    record Tree(int height, Boolean isVisible, int scenicScore) {
        Tree(int height, Boolean isVisible) {
            this(height, isVisible, 0);
        }

        public Tree withVisibility() {
            return new Tree(height, true, scenicScore);
        }

        public Tree withScenicScore(int scenicScore) {
            return new Tree(height, isVisible, scenicScore);
        }
    }

    private void part1() {
        for (int i = 1; i < forest.length - 1; i++) {
            for (int j = 1; j < forest[i].length - 1; j++) {
                final boolean visibleFromLeft = maxHeight(getTreesLeft(i, j)) < forest[i][j].height;
                final boolean visibleFromRight = maxHeight(getTreesRight(i, j)) < forest[i][j].height;
                final boolean visibleFromTop = maxHeight(getTreesUp(i, j)) < forest[i][j].height;
                final boolean visibleFromBottom = maxHeight(getTreesDown(i, j)) < forest[i][j].height;

                if (visibleFromLeft || visibleFromRight || visibleFromTop || visibleFromBottom) {
                    forest[i][j] = forest[i][j].withVisibility();
                }
            }
        }

        long visibleCount = Stream.of(forest).flatMap(Arrays::stream).filter(t -> t.isVisible).count();

        System.out.println("Visible count: "+visibleCount);
    }

    private Tree[] getTreesRight(int i, int j) {
        return Arrays.copyOfRange(forest[i], j + 1, forest[i].length);
    }

    private Tree[] getTreesLeft(int i, int j) {
        return Arrays.copyOfRange(forest[i], 0, j);
    }

    private Tree[] getTreesUp(int i, int j) {
        return getTreesVertical(0, i, j);
    }

    private Tree[] getTreesDown(int i, int j) {
        return getTreesVertical(i + 1, forest.length, j);
    }

    private Tree[] getTreesVertical(int startInclusive, int i, int j) {
        return IntStream.range(startInclusive, i).mapToObj(k -> forest[k][j]).toArray(Tree[]::new);
    }

    private void part2() {
        int maxScore = 0;
        for (int i = 1; i < forest.length - 1; i++) {
            for (int j = 1; j < forest[i].length - 1; j++) {
                final int currentHeight = forest[i][j].height;
                final int topVisibleTrees = getVisibleTrees(reverseTrees(getTreesUp(i, j)), currentHeight);
                final int leftVisibleTrees = getVisibleTrees(reverseTrees(getTreesLeft(i, j)), currentHeight);
                final int rightVisibleTrees = getVisibleTrees(getTreesRight(i, j), currentHeight);
                final int downVisibleTrees = getVisibleTrees(getTreesDown(i, j), currentHeight);
                final int scenicScore = topVisibleTrees * leftVisibleTrees * rightVisibleTrees * downVisibleTrees;

                System.out.println(forest[i][j].height + " top: "+topVisibleTrees
                        + ", left: "+ leftVisibleTrees + ", right: " + rightVisibleTrees
                        + ", down: "+ downVisibleTrees + "; Score: " + scenicScore);

                maxScore = Math.max(scenicScore, maxScore);
            }
        }

        System.out.println("Max score is "+maxScore);
    }

    private int getVisibleTrees(Tree[] trees, int currentHeight) {
        int visibleTrees = 0;

        for (int k = 0; k < trees.length; k++) {
            visibleTrees++;

            if (trees[k].height >= currentHeight) {
                break;
            }
        }

        return visibleTrees;
    }

    // meh, java does not allow me to do this in a generic way
    private static Tree[] reverseTrees(Tree[] input) {
        ArrayList<Tree> topTreesList = new ArrayList<>(List.of(input));
        Collections.reverse(topTreesList);
        return topTreesList.toArray(Tree[]::new);
    }

    private static Integer maxHeight(Tree[] trees) {
        return Stream.of(trees).map(Tree::height).max(Integer::compare).get();
    }

    private Tree[][] getForest() throws FileNotFoundException {
        final Scanner scanner = new Scanner(input);

        final List<List<Tree>> forest = new ArrayList<>();

        boolean isFirstRow = true;
        while (scanner.hasNextLine()) {
            int[] row = scanner.nextLine().chars().map(Character::getNumericValue).toArray();

            final boolean isLastRow = !scanner.hasNextLine();
            final boolean finalIsFirstRow = isFirstRow;
            forest.add(
                    IntStream.range(0, row.length).mapToObj(i -> {
                        boolean isEdge = i == 0 || i == row.length - 1;
                        return new Tree(row[i], isEdge || finalIsFirstRow || isLastRow);
                    }).toList()
            );

            isFirstRow = false;
        }

        Tree[][] arrayForest = forest.stream().map(l -> l.toArray(Tree[]::new)).toArray(Tree[][]::new);

        System.out.println("Forest: "+ Arrays.deepToString(arrayForest));

        return arrayForest;
    }
}
