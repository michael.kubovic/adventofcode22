package com.michael.adventofcode22;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day5 {

    private final File input = new File("input/day5.txt");

    private final List<Stack<Character>> stacks = new ArrayList<>();

    public static void main(String[] args) throws FileNotFoundException {
        Day5 today = new Day5();
        today.part1();
    }

    private void part1() throws FileNotFoundException {
        final Scanner scanner = new Scanner(input);

        List<List<Character>> cols = new ArrayList<>();

        Pattern instructionPattern = Pattern.compile("^move (\\d+) from (\\d+) to (\\d+)$");

        boolean stackSetup = true;
        while(scanner.hasNextLine()) {
            String line = scanner.nextLine();

            if (line.isEmpty()) {
                stackSetup = false;

                fillStacks(cols);

                continue;
            }

            if (stackSetup) {
                if (line.charAt(1) == '1') {
                    continue;
                }
                parseCratesIntoColumns(cols, line);
            } else {
                // instuctions

                System.out.println("line: "+line);
                Matcher matcher = instructionPattern.matcher(line);
                if (!matcher.find()) {
                    throw new RuntimeException("Instructions unclear!");
                }
                int count = Integer.parseInt(matcher.group(1));
                int from = Integer.parseInt(matcher.group(2)) - 1;
                int to = Integer.parseInt(matcher.group(3)) - 1;


                Stack<Character> tmp = new Stack<>();
                for (int i = 0; i < count; i++) {
                    // part 1
                    // stacks.get(to).push(stacks.get(from).pop());

                    tmp.push(stacks.get(from).pop());
                }

                while (!tmp.empty()) {
                    stacks.get(to).push(tmp.pop());
                }

                printStacks();
            }

        }

        String topOfStacks = stacks.stream()
                .map(Stack::pop)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        System.out.println("top of stacks: "+topOfStacks);
    }

    private void fillStacks(List<List<Character>> cols) {
        for (List<Character> col : cols) {
            Stack<Character> stack = new Stack<>();
            stacks.add(stack);
            for (int i = col.size() - 1; i >= 0; i--) {
                Character crate = col.get(i);
                if (crate != ' ') {
                    stack.push(crate);
                }
            }
        }

        System.out.println("stack count: "+stacks.size());
        printStacks();
    }

    private void printStacks() {
        stacks.forEach(s -> System.out.println("stack: "+s));
    }

    private static void parseCratesIntoColumns(List<List<Character>> cols, String line) {
        for (int i = 0; i < (line.length()+1)/4; i++) {
            int start = 4*i;
            char crate = line.charAt(start + 1);
//                    System.out.println("i: " + i + ", start: " + start  +", crate: "+crate);

            List<Character> col;
            if (cols.size() < i + 1) {
                col = new ArrayList<>();
                cols.add(col);
            } else {
                col = cols.get(i);
            }
            col.add(crate);
//                    System.out.println("Col after adding: "+col);
        }
    }

    Day5() {

    }


}
