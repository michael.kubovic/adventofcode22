package com.michael.adventofcode22;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Day10 {
    final private File input = new File("input/day10.txt");
    private Integer x;
    private Integer cycle;
    private List<Integer> signals;

    private char[] crt = new char[6 * 40 + 1];

    public static void main(String[] args) throws FileNotFoundException {
        Day10 day10 = new Day10();
        day10.part1();
    }

    private void part1() throws FileNotFoundException {
        Scanner scanner = new Scanner(input);

        x = 1;
        cycle = 1;
        signals = new ArrayList<>();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();

            if (line.equals("noop")) {
                cycle();
                continue;
            }

            String[] op = line.split(" ");
            if (op[0].equals("addx")) {
                int opValue = Integer.parseInt(op[1]);

                cycle();
                cycle();

                x = x + opValue;
            }
        }

        readSignal();

        System.out.println("Signals: " + signals);
        System.out.println("Sum of signals: " + signals.stream().reduce(Integer::sum).get());
    }

    private void cycle() {
        readSignal();
        render();

        cycle++;
    }

    private void render() {
        int spriteFrom = x - 1;
        int spriteTo = x + 1;

        int horizontalPos = ((cycle - 1) % 40);
        crt[cycle - 1] = (horizontalPos >= spriteFrom && horizontalPos <= spriteTo) ? '#' : '.';

        System.out.println("Current CRT row:");

        for (int i = 0; i < cycle / 40; i++) {
            System.out.println(new String(Arrays.copyOfRange(crt, i * 40, (i+1) * 40)));
        }
    }

    private void readSignal() {
        if (cycle == 20 || (cycle > 20 && (cycle - 20) % 40 == 0)) {
            System.out.println("Reading signal at cycle " + cycle +": "+ x);

            signals.add(cycle * x);
        }
    }
}
